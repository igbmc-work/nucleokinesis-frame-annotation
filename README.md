# Nucleokinesis semi-automatic annotation macro

This macro allows for semi-automatic annotation of front and rear regions of a nucleus in a tracked stack obtained from TrackMate.

## INSTRUCTIONS

0. Open the macro, by drag and drop into the main Fiji window
1. Open the stack you want to annotate, select it then run the macro
2. Move or Redraw the first circle, then press T to add it to ROI Manager &rarr; the macro will rename the first one as the REAR annotation
3. Move or Redraw the second circle, press T &rarr; the macro will rename it as the FRONT annotation
The macro will keep track of the frame in which the circles were added and rename the annotation with the **frame number**.
4. The macro will then advance to the next frame and allows for continue annotations
5. You can use the scroll wheel to move around the stack if you want to skip frames, the macro will keep track of the current frame and rename the ROIs accordingly
6. Please press Escape when you want to stop annotating. The Macro will NOT stop by itself.

## OUTPUT

- After adding all the ROIs to the ROI Manager, you can export them with `More>>` &rarr; `Save...`
- You can obtain the Mean signal measurements by selecting all ROIs (CTRL + A) and pressing `Measure`. If you don't get the Mean intensity in the Measurements Table, check that `Mean gray value` is checked in `Analyze` &rarr; `Set Measurements...`

## Structure

- data &rarr; example of tracked stack to annotate
- script &rarr; contains the Fiji macros for the project

## Author

Marco Dalla Vecchia dallavem@igbmc.fr
